# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0: return None

    result = sorted(values, reverse=True)

    return result[0]


print(max_in_list([1,55,9,36]))
