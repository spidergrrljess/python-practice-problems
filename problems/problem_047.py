# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    specials = '$!@'

    if len(password) < 6 or len(password) > 12: return "Fail"

    for lower in password:
        if lower.islower():
            for upper in password:
                if upper.isupper():
                    for num in password:
                        if num.isdigit():
                            for spec in password:
                                if spec in specials:
                                    return "Pass"
    else: return "Fail"



print(check_password("Abcc12!"))
